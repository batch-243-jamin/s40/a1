const Course = require("../models/Course");
const auth =require("../auth");


/*
	Steps:
	1. Create a new Course object using mongoose model and information from request of the user
	2. Save the new course to the database.
*/
module.exports.addCourse = (request, response) =>{
		
		let token = request.headers.authorization;
		const userData = auth.decode(token);
		console.log(userData);


		let newCourse = new Course({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			slots: request.body.slots
		})


		if(userData.isAdmin){
			newCourse.save().then(result => {
				console.log(result);
				response.send(true);
			}).catch(error => {
				console.log(error);
				response.send(false);
			})
		}
		else{
			response.send("You do not have an Admin role!")
		}

		
}

// Retrieve all active courses

module.exports.getAllActive = (request, response) =>{
	return Course.find({isActive: true})
	.then(result => {
		response.send(result);
	}).catch(err => {
		response.send(err);
	})
}

// retrieving a specific course

module.exports.getCourse = (request, response) => {
	const courseId = request.params.courseId;

	return Course.findById(courseId).then(result =>{
		response.send(result);
	}).catch(err =>{
		response.send(err);
	})

}

// archive a course

module.exports.archiveCourse = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	let archiveACourse = {
			isActive: request.body.isActive
		}

	const courseId = request.params.courseId;

	if(userData.isAdmin){
		return Course.findById(courseId, archiveACourse, {new:true}).then(result => {
			response.send(true)
		}).catch(error =>{
			response.send(error);
		})
	}
	else{
		return response.send("You don't have access to this page!");
	}

}

// update a course

module.exports.updateCourse = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	console.log(userData);

		let updatedCourse = {
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			slots: request.body.slots
		}

	const courseId = request.params.courseId;

	if(userData.isAdmin){
		return Course.findByIdAndUpdate(courseId, updatedCourse, {new:true}).then(result => {
			response.send(result)
		}).catch(err =>{
			response.send(err);
		})
	}else{
		return response.send("You don't have access to this page!");
	}
}