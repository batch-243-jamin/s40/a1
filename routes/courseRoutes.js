const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth.js");

// Adding course
router.post("/", auth.verify, courseControllers.addCourse);

// All active courses
router.get("/allActiveCourses", courseControllers.getAllActive);

// Speficific course
router.get("/:courseId", courseControllers.getCourse);

// update specific course
router.put("/update/:courseId", auth.verify, courseControllers.updateCourse);

// archive a course
router.patch("/:courseId/archive", auth.verify, courseControllers.archiveCourse);

module.exports = router;